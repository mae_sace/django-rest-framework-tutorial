from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework import renderers
from rest_framework.routers import DefaultRouter

from .views import SnippetViewSet, UserViewSet, api_root

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'snippets', SnippetViewSet)
router.register(r'users', UserViewSet)


# The API URLs are now determined automatically by the router.
urlpatterns = [
    url(r'^', include(router.urls)),
]